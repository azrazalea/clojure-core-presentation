#!/bin/sh

# You will need to install pdflatex binary and the upquote latex package to use
# the instructions for this are very environment specific
pdflatex --shell-escape presentation.tex
